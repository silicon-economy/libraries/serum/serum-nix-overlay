# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

self: super: {
  gitlab-clippy = self.callPackage ./gitlab-clippy.nix {};
  cargo2junit = self.callPackage ./cargo2junit.nix {};
  check-sele-licenses = self.callPackage ./check-sele-licenses.nix {};
}
