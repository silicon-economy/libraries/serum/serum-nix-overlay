# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

{ lib, rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "cargo-check-sele-licenses";
  version = "0.0.0";

  src = fetchGit {
    url = "https://git.openlogisticsfoundation.org/silicon-economy/libraries/serum/check-sele-licenses.git";
    allRefs = true;
    rev = "a140bb1506b837225ffe352fd2ca4c2f5b6554e6";
  };

  cargoHash = "sha256-+hoWYb7Xi/XYNXpJMVjHo+oeVl6tqMzDmJYjvjsTtHM=";
  #doCheck = false;

  meta = with lib; {
    description = "Simple executable to check the licenses of all dependencies in a cargo project or workspace";
    homepage = "https://gitlab.cc-asp.fraunhofer.de/silicon-economy/libraries/serum/check-sele-licenses";
    #license = with licenses; [ mit ];
    #platforms = [ "x86_64-linux" ];
  };
}
