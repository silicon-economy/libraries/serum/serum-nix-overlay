# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

{ lib, rustPlatform, fetchFromGitLab }:

rustPlatform.buildRustPackage rec {
  pname = "cargo-gitlab-clippy";
  version = "1.0.3";

  src = fetchFromGitLab {
    owner = "dlalic";
    repo = "gitlab-clippy";
    rev = version;
    sha256 = "0rnbr0jpsr2srhmn45brbr458ippkmdvwj4qh2kq8mw80naadd3p";
  };

  cargoHash = "sha256-byAdB1Evcg0wERS8e6hR350naPxen2V5YaFydsiV2DU=";
  doCheck = false;

  meta = with lib; {
    description = "Convert clippy warnings into GitLab Code Quality report";
    homepage = "https://gitlab.com/dlalic/gitlab-clippy";
    license = with licenses; [ mit ];
    #maintainers = with maintainers; [];
    #platforms = [ "x86_64-linux" ];
  };
}
