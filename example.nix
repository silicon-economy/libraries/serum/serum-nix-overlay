# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

# This example shows how to use the overlay to set up an environment for embedded Rust development.
# It uses a pinned nixpkgs version and the Mozilla overlay to get the appropriate Rust toolchain.
# The example is intentionally kept simple and does not set up a C cross compiler or additional
# (cross) system libraries which is where Nix shines the most. Cross-compilation is handled by
# Rust itself here instead of by Nix.

let
  # Mozilla's Rust overlay gives us the rustChannelOf function which allows us
  # to select a specific Rust toolchain. Furthermore, we can configure
  # additional targets like shown below.
  moz_overlay = import (builtins.fetchTarball {
      name = "mozilla-overlay-2022-07-07";
      #url = "https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz";
      url = "https://github.com/mozilla/nixpkgs-mozilla/archive/0508a66e28a5792fdfb126bbf4dec1029c2509e0.tar.gz";
      sha256 = "1nswjmya72g0qriidc2pkl54zn5sg0xp36vdq0ylspca56izivxc";
    });
  # Our own overlay with additional build environment tools
  serum_overlay = import (builtins.fetchGit {
      name = "serum-overlay-2022-08-03";
      url = "git@gitlab.cc-asp.fraunhofer.de:silicon-economy/libraries/serum/serum-nix-overlay.git";
      allRefs = true;
      rev = "d060d4d1f1067aab302259b4f932ae3ea77a0644"; # Use the newest version here
    });
  # Pinned nixpkgs
  nixpkgsBase = import (builtins.fetchTarball {
      name = "nixpkgs-stable-22.05";
      url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/22.05.tar.gz";
      sha256 = "0d643wp3l77hv2pmg2fi7vyxn4rwy0iyr8djcw1h5x72315ck9ik";
    });
  nixpkgs = nixpkgsBase {
      overlays = [ moz_overlay serum_overlay ];
    };
  # Choose between a specific Rust channel and 'latest' and choose (cross-compilation) targets
  #rustChannel = nixpkgs.latest.rustChannels.stable;
  rustChannel = nixpkgs.rustChannelOf { channel = "1.64.0"; };
  rust = rustChannel.rust.override {
    targets = [ "thumbv7em-none-eabihf" ];
  };
  # As of 2022-03-03, tarpaulin only supports x86-64 Linux
  optionalCargoTarpaulin = with nixpkgs;
    if stdenv.hostPlatform.isx86_64 && stdenv.hostPlatform.isLinux
      then [ cargo-tarpaulin ]
      else [];
in
  nixpkgs.stdenv.mkDerivation {
    name = "rust-env";
    buildInputs = [
      rust
      nixpkgs.gitlab-clippy
      nixpkgs.cargo2junit
      nixpkgs.check-sele-licenses
      nixpkgs.git # required when https://doc.rust-lang.org/cargo/reference/config.html#netgit-fetch-with-cli is set
      nixpkgs.cacert # for certificate verification when downloading from crates.io
      nixpkgs.libiconv # for linking on macOS
    ] ++ optionalCargoTarpaulin;
  }
