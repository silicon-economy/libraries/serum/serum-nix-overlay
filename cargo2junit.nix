# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

{ lib, rustPlatform, fetchFromGitHub }:

rustPlatform.buildRustPackage rec {
  pname = "cargo-cargo2junit";
  version = "0.1.8";

  src = fetchFromGitHub {
    owner = "johnterickson";
    repo = "cargo2junit";
    rev = "v${version}";
    sha256 = "1gs1gs8yskz7czs6hm8ycy2srmp1c1qkhi4yivdsbc5craid6ih8";
  };

  cargoSha256 = "0qr21brp5szr0pm4kn2z90am75p889zn6najngjjss9vkhzl89b5";
  #doCheck = false;

  meta = with lib; {
    description = "Converts cargo's json output (from stdin) to JUnit XML (to stdout)";
    homepage = "https://github.com/johnterickson/cargo2junit";
    license = with licenses; [ mit ];
    #maintainers = with maintainers; [];
    #platforms = [ "x86_64-linux" ];
  };
}
