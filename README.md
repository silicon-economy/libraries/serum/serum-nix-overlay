# serum-nix-overlay

This nix overlay contains additional build environment tools for embedded Rust development.

As of 2022-08-03, these are:
- [check-sele-licenses](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/libraries/serum/check-sele-licenses) to validate all dependencies' licenses with the [Silicon Economy allowlist](https://oe160.iml.fraunhofer.de/wiki/pages/viewpage.action?spaceKey=HOW&title=Open+Source+and+Data+Licenses#OpenSourceandDataLicenses-Standardlicenses)
- [GitLab Clippy](https://gitlab.com/dlalic/gitlab-clippy) to convert clippy warnings into GitLab Code Quality report
- [cargo2junit](https://github.com/johnterickson/cargo2junit) to convert `cargo test`'s json output to JUnit xml

For a usage example, see `example.nix` which can be instantiated with `nix-shell example.nix`.

## License

Open Logistics Foundation License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contact

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
